export interface ICabinet {
    id?:string;
    nom: string;
    logo: string;
    email: string;
    adresse: string;
    telephone: string;
  }
  