import React, { ReactNode } from 'react';

import { Avatar, Tag, Table } from 'antd';

import { ColumnProps } from 'antd/es/table';
import { IRole } from '../../../interfaces/roles';

const columns: ColumnProps<IRole>[] = [

  {
    key: 'nom',
    dataIndex: 'nom',
    title: 'Nom',
    sorter: (a, b) => (a.nom > b.nom ? 1 : -1),
    render: (nom) => <strong>{nom}</strong>
  },
  {
    key: 'droit_acces',
    dataIndex: 'droit_acces',
    title: 'Droit d acces',
    sorter: (a, b) => (a.droit_acces > b.droit_acces ? 1 : -1),
    render: (droit_acces) => <strong>{droit_acces}</strong>
  },
  {}
];

type Props = {
  data: IRole[];
  actions?: (Role: IRole) => ReactNode;
};

const RolesTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<IRole> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  ); 
};

export default RolesTable;
