export interface IUserApp {
    id?: string;
    nom: string;
    prenom?: string;
    roles?: string;
    email: string;
    telephone?: string;
    cabinet?: string;
    password?: string;
  }