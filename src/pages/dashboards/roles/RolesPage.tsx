import React, { useState } from 'react';
import { Button } from 'antd';
import EditRole from './EditRole';
import AddRole from './AddRole';
import RolesTable from '../../../layout/components/roles/RolesTable';
import PageAction from '../../../layout/components/page-action/PageAction';
import { useFetchPageData, usePageData } from '../../../hooks/usePage';
import { IRole } from '../../../interfaces/roles';
import { IPageData } from '../../../interfaces/page';
import axios from 'axios';
import { getToken } from '../../../utils/common';
 
const pageData: IPageData = {
  title: 'Roles',
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboard',
      route: 'default-dashboard'
    },
    {
      title: 'Roles'
    }
  ]
};
const RolesPage = () => {
  usePageData(pageData);
  const [Roles, setRoles] = useFetchPageData<IRole[]>(
    './data/roles.json',
    []
    );   
  const [selectedRole, setSelectedRole] = useState(null);
  const [addingModalVisibility, setAddingModalVisibility] = useState(false);
  const handleDelete = (Role: IRole) => {
   /* let token = getToken();
    axios.put(`http://127.0.0.1:8000/api/Roles/${Role.id}`,{
      "deleted": true
    },{
    headers: { 
    'content-type': 'application/json',
    'Authorization': `Bearer ${token}`
  }});*/
    const newRoles = Roles.filter((el) => el !== Role);
    setRoles(newRoles);
  };
  const handleEdit = (Role: IRole) => {
    const editedRoles = Roles.map((el) =>
      el !== selectedRole ? el : Role
    );
    setRoles(editedRoles);
    setSelectedRole(null);
  };
  const openAddingModal = () => setAddingModalVisibility(true);
  const addRole = (Role: IRole) => {
    setRoles([Role, ...Roles]);
    setAddingModalVisibility(false);
  };
  const closeAddingModal = () => setAddingModalVisibility(false);
  const openEditModal = (Role: IRole) => setSelectedRole(Role);
  const closeModal = () => {
    setSelectedRole(null);
  };
  const RolesActions = (Role: IRole) => (
    <div className='buttons-list nowrap'>
      <Button onClick={openEditModal.bind({}, Role)} shape='circle' type='primary'>
        <span className='icofont icofont-edit-alt' />
      </Button>
      <Button onClick={handleDelete.bind({}, Role)} shape='circle' danger>
        <span className='icofont icofont-ui-delete' />
      </Button>
    </div>
  );
  return (
    <>
      <PageAction onClick={openAddingModal} icon='icofont-plus' type={'primary'} />
      <RolesTable data={Roles} actions={RolesActions} />
      <AddRole
        onClose={closeAddingModal}
        visible={addingModalVisibility} 
        onSubmit={addRole}
      />
      <EditRole
        Role={selectedRole}
        visible={!!selectedRole}
        onClose={closeModal}
        onEdited={handleEdit}
      />
    </>
  );
};
export default RolesPage;
