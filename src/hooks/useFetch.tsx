import { useEffect, useState } from 'react';
import { getToken } from '../utils/common';

import axios from 'axios';

export function useFetch<T>(
  url: string,
  initialState: T = null,
  callback?: (T) => any
): [T, (data: T) => void] {
  const [data, setData] = useState<T>(initialState);

  const token=getToken();
  async function getData() {
    const result = await axios.get(url,{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
    return result.data['hydra:member'];
  }

  useEffect(() => {
    getData()
      .then((data) => {
        if (callback) {
          callback(data);
        }

        setData(data);
      })
      .catch(console.error);
  }, [url]);

  return [data, setData];
}
