export interface IRole {
    id?: string;
    nom: string;
    droit_acces: string;
  }