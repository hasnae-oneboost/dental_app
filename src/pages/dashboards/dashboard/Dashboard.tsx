import React from 'react';

import { Card } from 'antd';




import { usePageData } from '../../../hooks/usePage';

import { IPageData } from '../../../interfaces/page';

const pageData: IPageData = {
  fulFilled: false,
  breadcrumbs: [
    {
      title: 'Dashboards',
      route: 'default-dashboard'
    },
    {
      title: 'Default'
    }
  ]
};

const DashboardPage = () => {
  usePageData(pageData);

  return (
    <>
      <p>Welcome </p>
    </>
  );
};

export default DashboardPage;
