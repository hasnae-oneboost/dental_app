import React from 'react';
import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import UserAppForm from './UserAppForm';
import { IUserApp } from '../../../interfaces/userApps';
type Props = {
  onSubmit: (UserApp: IUserApp) => void;
  visible: boolean;
  onClose: () => void;
};

const AddUserApp = ({ visible, onClose, onSubmit }: Props) => {
 const token = getToken();
 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <UserAppForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddUserApp;
