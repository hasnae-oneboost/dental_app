import { IRoute } from '../interfaces/routing';

import SettingsPage from '../pages/settings/Settings';

import UsersPage from '../pages/dashboards/users/UsersPage';
import RolesPage from '../pages/dashboards/roles/RolesPage';
import UsersAppPage from '../pages/dashboards/users_app/UsersAppPage';
import DashboardPage from '../pages/dashboards/dashboard/Dashboard';
import CabinetsPage from '../pages/dashboards/cabinets/CabinetsPage'; 
import TypographyPage from '../pages/typography/TypographyPage';
import TablesPage from '../pages/tables/TablesPage';
import AntdIconsPage from '../pages/icons/AntdIconsPage';
import IconsOptionsPage from '../pages/icons/IconsOptionsPage';
import IcofontIconsPage from '../pages/icons/IcofontIconsPage';

export const defaultRoutes: IRoute[] = [
  {
    path: 'settings',
    component: SettingsPage
  },
  {
    path: 'users',
    component: UsersPage
  },
  {
    path: 'users_app',
    component: UsersAppPage
  },
  {
    path: 'default-dashboard',
    component: DashboardPage
  },
  {
    path: 'cabinets',
    component: CabinetsPage
  },
  {
    path: 'roles',
    component: RolesPage
  },
  {
    path: 'tables',
    component: TablesPage
  },
  {
    path: 'ant-icons',
    component: AntdIconsPage
  },
  {
    path: 'icons-options',
    component: IconsOptionsPage
  },
  {
    path: 'icofont-icons',
    component: IcofontIconsPage
  }
];
