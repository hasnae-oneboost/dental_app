import React from 'react';
import { IRole } from '../../../interfaces/roles';
import RoleForm from './RoleForm';
import { Modal } from 'antd';

type Props = {
  onEdited: (IRole) => void;
  Role: IRole;
  visible: boolean;
  onClose: () => void;
};

const EditRole = ({ Role, visible, onClose, onEdited }: Props) => {
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Modifier Role</h3>}
    >
      <RoleForm
        onCancel={onClose}
        onSubmit={onEdited}
        Role={Role}
        submitText='Modifier'
      />
    </Modal>
  );
};

export default EditRole;
