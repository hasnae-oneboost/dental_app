import React from 'react';
import { getToken } from '../../../utils/common';
import { Modal } from 'antd';

import RoleForm from './RoleForm';
import { IRole } from '../../../interfaces/roles';
type Props = {
  onSubmit: (Role: IRole) => void;
  visible: boolean;
  onClose: () => void;
};

const AddRole = ({ visible, onClose, onSubmit }: Props) => {
 const token = getToken();
 
  return (
    <Modal
      visible={visible}
      onCancel={onClose}
      destroyOnClose
      footer={null}
      title={<h3 className='title'>Ajouter</h3>}
    >
      <RoleForm onCancel={onClose} onSubmit={onSubmit} submitText='Ajouter' />
    </Modal>
  );
};

export default AddRole;
