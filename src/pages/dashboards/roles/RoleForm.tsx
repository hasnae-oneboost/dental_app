import React, { useEffect, useState } from 'react';

import { Button, Select, Input, AutoComplete } from 'antd';

import { useFetch } from '../../../hooks/useFetch';

import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';

import { IRole } from '../../../interfaces/roles';
//import { AnyARecord } from 'dns';

type Props = {
  Role?: IRole;
  onSubmit: (Role: IRole) => void;
  onCancel: () => void;
  submitText?: string;
};


const defaultSubmitText = 'Ajouter';
const emptyRole = {
    nom: '',
    droit_acces: ''
};

const token = getToken();
const RoleSchema = Yup.object().shape({
  nom: Yup.string().required(),
  droit_acces: Yup.string().required()
});

function refreshPage(){ 
  window.location.reload(); 
}

const RoleForm = ({
  submitText = defaultSubmitText,
  Role = emptyRole,
  onSubmit,
  onCancel
}: Props) => {
  //const [cabinets] = useFetch<{ value: any }[]>('http://127.0.0.1:8000/api/cabinets?deleted=false', []);


  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IRole>({
    validationSchema: RoleSchema,
    initialValues: Role,
    onSubmit: (form) =>{
        onSubmit({ ...form });
        resetForm();
    }
});
  

     
  useEffect(() => {
    setValues({ ...values });
  }, [Role]);

  /*const handleCabinetSelect = (cabinet) => {
    setValues({ ...values, cabinet });
  };*/

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
 
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
       

        <div className='form-group'>
          <Input
            name='nom'
            placeholder='Nom'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.nom}
            className={hasError('nom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.droit_acces}
            placeholder='droit_acces'
            onBlur={handleBlur}
            name='droit_acces'
            onChange={handleChange}
            className={hasError('droit_acces')}
          />
        </div>

      

        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Cancel
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};

export default RoleForm;
