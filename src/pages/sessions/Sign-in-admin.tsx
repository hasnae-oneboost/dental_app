import React, {useState} from 'react';
import { Button, Form, Input, Switch } from 'antd';
import { LoginOutlined } from '@ant-design/icons/lib';
import PublicLayout from '../../layout/public/Public';
import { Link } from 'react-router-dom';
import { useForm } from 'antd/es/form/Form';
import { navigateHome } from '../../utils/naviagate-home';
import axios from 'axios';
import { setUserSession } from '../../utils/common';

const { Item } = Form;
const SignInAdmin = () => {
  const [form] = useForm();
  const [email,setEmail] = useState("");
  const [password,setPassword] = useState("");
  const [error, setError] = useState(null);
  
      const login = () => {
        setError(null);
      
        axios.post('http://127.0.0.1:8000/api/login', { email, password })
        .then(response => {
          setUserSession(response.data.token, response.data.user);
          navigateHome();
        }).catch(error => {        
          if (error.response.status === 401)
           setError("Informations incorrectes");
          else setError("Something went wrong. Please try again later.");
        });
      }
  return (
    <PublicLayout bgImg={`${window.origin}/content/login-page.jpg`}>
      <h4 className='mt-0 mb-1'>Se Connecter en Tant Qu'Administrateur</h4>
      <p className='text-color-200'>Login to access your Account</p>
      {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br />
      <Form form={form} layout='vertical' className='mb-4'>
        <Item name='email' rules={[{ required: true, message: <></> }]}>
          <Input placeholder='Email' onChange={(e)=>setEmail(e.target.value)} />
        </Item>
        <Item name='password' rules={[{ required: true, message: <></> }]}>
          <Input placeholder='Password' type='password' onChange={(e)=>setPassword(e.target.value)} />
        </Item>
      
        <div className='d-flex align-items-center mb-4'>
          <Switch defaultChecked /> <span className='ml-2'>Remember me</span>
        </div>
        <Button
          block={false}
          type='primary'
          onClick={login}
          htmlType='submit'
          icon={<LoginOutlined style={{ fontSize: '1.3rem' }} />}
        >
          Login
        </Button>

        
      </Form>
      <br />
      <p className='mb-1'>
        <a href='#'>Forgot password?</a>
      </p>
      <p>
        Se connecter en tant qu'utilisateur <Link to='/'>Se Connecter!</Link>
      </p>
    </PublicLayout>
  );
};
  

export default SignInAdmin;