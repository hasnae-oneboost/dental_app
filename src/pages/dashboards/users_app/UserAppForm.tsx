import React, { useEffect, useState } from 'react';

import { Button, Select, Input, AutoComplete } from 'antd';

import { useFetch } from '../../../hooks/useFetch';


import { getToken } from '../../../utils/common';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import axios from 'axios';
import { hasErrorFactory } from '../../../utils/hasError';

import { IUserApp } from '../../../interfaces/userApps';
//import { AnyARecord } from 'dns';
 
type Props = {
  UserApp?: IUserApp;
  onSubmit: (UserApp: IUserApp) => void;
  onCancel: () => void;
  submitText?: string;
};

const defaultSubmitText = 'Ajouter';
const emptyUserApp = {
    nom: '',
    prenom: '',
    email: '',
    roles: '',
    cabinet: '',
    telephone: '',
    password: ''
};

const token = getToken();
const UserAppSchema = Yup.object().shape({
  nom: Yup.string().required(),
  prenom: Yup.string().required(),
  email: Yup.string().required(),
  roles: Yup.string().required(),
  cabinet: Yup.string().required(),
  telephone: Yup.string().required(),
  password: Yup.string().required()
});

function refreshPage(){ 
  window.location.reload(); 
}

const UserAppForm = ({
  submitText = defaultSubmitText,
  UserApp = emptyUserApp,
  onSubmit,
  onCancel
}: Props) => {
  const [cabinets] = useFetch<{ value: any }[]>('http://127.0.0.1:8000/api/cabinets?deleted=false', []);


  const {
    handleSubmit ,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    values,
    setValues,
    isValid,
    errors,
    touched,
    resetForm
  } = useFormik<IUserApp>({
    validationSchema: UserAppSchema,
    initialValues: UserApp,
    onSubmit: (form) =>{
      if(submitText=='Ajouter')
    {
      axios.post('http://127.0.0.1:8000/api/UserApps',{
     //"id":values.id,
     "email":values.email,
     "nom": values.nom,
      "prenom": values.prenom,
      "cabinet": values.cabinet,
      "telephone": values.telephone,
      "password": values.password,
      "deleted":false
    },
      {
        headers: { 
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
      })    
      onSubmit({ ...form });
      resetForm();
   }
   else  if(submitText=='Modifier')
    {
    axios.put(`http://127.0.0.1:8000/api/UserApps/${UserApp.id}`,{
      "email":values.email,
       "nom": values.nom,
        "prenom": values.prenom,
        "cabinet": values.cabinet,
        "telephone": values.telephone,
        "password": values.password,
        "deleted":false
     },{
      headers: { 
      'content-type': 'application/json',
      'Authorization': `Bearer ${token}`
    }})
   onSubmit({ ...form });
      resetForm();
  }
}});
     /*const res =axios.post('http://127.0.0.1:8000/api/UserApps',{
       "email":values.email,
       "nom": values.nom,
        "prenom": values.prenom,
        "cabinet": values.cabinet,
        "telephone": values.telephone,
        "password": values.password
     },{
      headers: {
          'content-type': 'application/json',
          'Authorization': `bearer ${token}`
        }})

        //console.log(res)
          onSubmit({...form});      
          resetForm();
      }
  });  */
     
  useEffect(() => {
    setValues({ ...values });
  }, [UserApp]);

  const handleCabinetSelect = (cabinet) => {
    setValues({ ...values, cabinet });
  };
 //const handleStatusSelect = (value) => setFieldValue('status', value);

  const handleCancel = () => {
    resetForm();
    onCancel();
  };
 
  const hasError = hasErrorFactory(touched, errors);

  return (
    <>
      <form onSubmit={handleSubmit}>
       

        <div className='form-group'>
          <Input
            name='nom'
            placeholder='Nom'
            onBlur={handleBlur}
            onChange={handleChange}
            defaultValue={values.nom}
            className={hasError('nom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.prenom}
            placeholder='Prenom'
            onBlur={handleBlur}
            name='prenom'
            onChange={handleChange}
            className={hasError('prenom')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.email}
            placeholder='Email'
            name='email'
            type='email'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('email')}
          />
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.password}
            placeholder='Mot de passe'
            name='password'
            type='password'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('password')}
          />
        </div>
        <div className='form-group'>
          <Select
            placeholder='Cabinet'
            defaultValue={values.cabinet}
            onChange={handleCabinetSelect}
            className={hasError('cabinet')}
            onBlur={() => setFieldTouched('cabinet')}
          >
            {               
              cabinets.map((cabinet)=>(
            <Select.Option value={cabinet['@id']}>{cabinet['nom']}</Select.Option>
              ))
            }
          </Select>
        </div>

        <div className='form-group'>
          <Input
            defaultValue={values.roles}
            placeholder='Role'
            name='roles'
            type='roles'
            onBlur={handleBlur}
            onChange={handleChange}
            className={hasError('roles')}
          />
        </div>
          
    
        <div className='form-group'>
          <Input
            type='telephone'
            name='telephone'
            onBlur={handleBlur}
            placeholder='Telephone'
            onChange={handleChange}
            defaultValue={values.telephone}
            className={hasError('telephone')}
          />
        </div>

        <div className='d-flex justify-content-between buttons-list settings-actions'>
          <Button danger onClick={handleCancel}>
            Cancel
          </Button>
          <Button disabled={!isValid} type='primary' htmlType='submit'>
            {submitText}
          </Button>
        </div>
      </form>
    </>
  );
};

export default UserAppForm;
