import React, { ReactNode } from 'react';

import { Avatar, Table } from 'antd';

import { ColumnProps } from 'antd/es/table';
import { ICabinet } from '../../../interfaces/cabinets';

const Cabinetlogo = ({ logo }) => {
  const isData = logo.startsWith('data:image');
  const isWithPath = logo.startsWith('http');

  if (isData || isWithPath) {
    return <Avatar size={40} src={logo} />;
  }

  return <Avatar size={40} src={`${window.location.origin}/${logo}`} />;
};
 
const columns: ColumnProps<ICabinet>[] = [
   

  {
    key: 'logo',
    title: 'Logo',
    dataIndex: 'logo',

    render: (logo) => <Cabinetlogo logo={logo} />
  },
  {
    key: 'nom',
    dataIndex: 'nom',
    title: 'Nom',
    sorter: (a, b) => (a.nom > b.nom ? 1 : -1),
    render: (nom) => <strong>{nom}</strong>
  },
  {
    key: 'email',
    dataIndex: 'email',
    title: 'Email',
    sorter: (a, b) => (a.email > b.email ? 1 : -1),
    render: (email) => (
      <span className='nowrap' style={{ color: '#336cfb' }}>
        <span className='icofont icofont-ui-email mr-1' style={{ fontSize: 16 }} />
        {email}
      </span>
    )
  },
 
  {
    key: 'telephone',
    dataIndex: 'telephone',
    title: 'Telephone',
    render: (telephone) => (
      <span className='d-flex align-baseline nowrap' style={{ color: '#336cfb' }}>
        <span className='icofont icofont-ui-cell-phone mr-1' style={{ fontSize: 16 }} />
        {telephone}
      </span>
    )
  },
  { key: 'adresse', title: 'Adresse', dataIndex: 'adresse' },
  {}
];

type Props = {
  data: ICabinet[];
  actions?: (Cabinet: ICabinet) => ReactNode;
};

const CabinetsTable = ({ data, actions }: Props) => {
  const actionColumn: ColumnProps<ICabinet> = {
    key: 'actions',
    title: 'Actions',
    render: actions
  };

  const displayedColumns = actions ? [...columns, actionColumn] : columns;

  return (
  
    
    <Table
      rowKey='number'
      dataSource={data}
      columns={displayedColumns}
      pagination={{ hideOnSinglePage: true }}
    />
  );
};

export default CabinetsTable;
